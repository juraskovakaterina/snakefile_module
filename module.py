# def hello():
#   print("Hello world")

import pandas as pd
from snakemake.utils import min_version

min_version("5.18.0")

sample_tab = pd.DataFrame.from_dict(config["samples"],orient="index")

if not config["is_paired"]:
    read_pair_tags = ["SE"]
else:
    read_pair_tags = ["R1","R2"]

wildcard_constraints:
    sample = "|".join(sample_tab.sample_name),
    read_pair_tag = "R1|R2|SE"