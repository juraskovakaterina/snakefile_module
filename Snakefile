from snakemake.utils import min_version
min_version("5.18.0")

rule all:
    conda:  "env.yaml"
    script: "script.py"
